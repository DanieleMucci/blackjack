This is a simple Server application for blackjack.

EXECUTION

The server uses SSL sockets; therefore a certificate is needed. 
The certificate is /src/main/resources/cert.cer and it is contained in the keystore /src/main/resources/keystore.ks
The password to unlock the keystore is hardcoded in Server's code, but the keystore must be manually added at runtime.

To launch the server is is necessary to:
$mvn install
$cd target
$java -Djavax.net.ssl.keyStore=classes/keystore.ks -jar DanieleMucciBlackjack-1.0.jar


ADDITIONAL INFORMATION

The communication protocol between client and server is very simple. 

There are 3 types of messages:

REQT - Request messages
REST - Result messages
DATA - Data messages

REQT messages can have the following payloads:

NAME - The server can ask the client for the user's name
BET  - The server can ask the client for the user's bet
COMMAND - The server can ask the client for the user's command (STAND, HIT, DOUBLE)
COMMANDNODOUBLE - The server can ask the client for the user's command (STAND or HIT)
NEW_GAME - The server can ask the client for the users' decision to play a new game

REST messages can have the following payloads:

WIN - The user won the game
LOSE - The user lost the game
BJ - The user won the game with a blackJack

DATA messages can have the following payloads:

card_string - A custome string from the server to show the cards
command - H S or D . It is sent from the client to answer REQTCOMMAND message
YES - The user wants to play a new game
NO - The user doesn't want to play a new game


