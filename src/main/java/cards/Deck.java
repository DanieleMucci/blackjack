package cards;

import java.util.ArrayList;
import java.util.Collections;

/**
 * An implementation of the deck of cards (consisting of more than an individual
 * deck )
 * 
 * @author Daniele Mucci
 *
 */
public class Deck {

	private ArrayList<Card> cards;
	private int numCards;
	private int numDecks;

	/**
	 * Default constructor. Returns a deck composed of just 1 Deck.
	 */
	
	public Deck() {
		 this(1);
	}
	/**
	 * Constructor which takes as parameter the number of individual decks which compose the global deck (usually 4 , 6 or 8) and shuffles it
	 * @param numDecks the number of individual decks 
	 */
	public Deck(int numDecks) {
		this.numDecks=numDecks;
		this.numCards = 52 * numDecks;
		cards = new ArrayList<Card>();

		for (int d = 0; d < numDecks; d++) { 					//For each deck
			for (int s = 0; s < 4; s++) {						//For each suit
				for (int n = 1; n < 14; n++) {					//For each number
					cards.add(new Card(Suits.values()[s], n));
				}
			}
		}
		//Shuffle the deck
		Collections.shuffle(this.cards);	
		
	}
	/**
	 * Deals the top card of the deck . If the deck has less than 10% of total card, resets it.
	 * @return the first card of the deck
	 */
	
	public Card dealCard(){
		Card returnCard=cards.remove(0);
		numCards--;
		if (numCards<(int)(52*numDecks*0.1)){
			cards = new ArrayList<Card>();

			for (int d = 0; d < numDecks; d++) { 					//For each deck
				for (int s = 0; s < 4; s++) {						//For each suit
					for (int n = 1; n < 14; n++) {					//For each number
						cards.add(new Card(Suits.values()[s], n));
					}
				}
			}
			//Shuffle the deck
			Collections.shuffle(this.cards);
		}
			
		return returnCard;
	}
	

}
