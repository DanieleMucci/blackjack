package cards;
/**
 * Implementation of a card object
 * @author Daniele Mucci
 *
 */
public class Card {

	private int valueCard;
	private Suits suit;

	/**
	 * Constructor for a new card
	 * 
	 * @param suit
	 *            the suit of the new card
	 * @param value
	 *            the numeric value of the card (1 (Ace) - 13 (King) )
	 */
	public Card(Suits suit, int value) {
		if (value > 0 && value < 14) {
			this.suit = suit;
			this.valueCard = value;
		} else {
			System.err.println(value + " is not a valid value for a card");
			System.exit(1);
		}
	}
	/** 
	 * Constructor which creates a new card by its representation (i.e. 10H , 5D )
	 * @param representation is the representation of the card in string form 
	 */
	public Card (String representation) {
		int l=representation.length();
		if (l<2 || l>3){
			System.err.println(representation+ " is not a valid format string");
			System.exit(1);
		}
		String s="";
		int v;
		if (l==3){
			if (representation.charAt(0)!='1'){
				System.err.println(representation+ " is not a valid format string");
				System.exit(1);
			}
			s=""+representation.charAt(2);
			v=10;
			
		}
		else {
			v=Integer.parseInt(""+representation.charAt(0));
			s=""+representation.charAt(1);
		}
		Suits newSuit=null;
		switch (s){
			case "H":
				newSuit=Suits.H;
			case "D":
				newSuit=Suits.D;
			case "S":
				newSuit=Suits.S;
			case "C":
				newSuit=Suits.C;
			default:
				System.err.println(representation+ " is not a valid format string");
				System.exit(1);
			break;
		}
		this.suit=newSuit;
		this.valueCard=v;
	}

	/**
	 * Returns the numeric value of the card
	 * 
	 * @return an integer representing the value of the card
	 */
	public int getValue() {
		if (this.valueCard>=10)
			return 10;
		else
			return this.valueCard;

	}
	/**
	 * Overriding of toString method. The desired representation is i.e. 10H , AC , JS 
	 */
	public String toString() {
		String value="";
		if (this.valueCard==1)
			value="A";
		else if (this.valueCard==11)
			value="J";
		else if (this.valueCard==12)
			value="Q";
		else if (this.valueCard==13)
			value="K";
		else 
			value=""+this.valueCard;
		String myStr=value+this.suit.toString();
		return myStr;
		
	}

}
