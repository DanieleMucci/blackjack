package player;

import java.util.ArrayList;

import cards.Card;

/**
 * Implementation of a player , with a name and a list of cards.
 * @author Daniele Mucci
 */
public class Player {

	protected String name;
	protected ArrayList<Card> hand;
	
	/**
	 * Default constructor which sets just the name of the player
	 * @param n is the name of the player
	 */
	
	public Player(String n){
		this.name=n;
		this.resetHand();
	}
	/**
	 * Returns the name of the player
	 * @return the String representing the name of the player
	 */
	public String getName(){
		return this.name;
	}
	/**
	 * Adds a card to the player's hand
	 * @param c is the card to add to the player's hand
	 */
	public void addCard(Card c){
		this.hand.add(c);
	}
	/**
	 * Returns the computed value of the hand of this player
	 * @return the blackjack value of the cards in player's hand
	 */
	public int getHandValue(){
		int counter=0;
		boolean acePresent=false;
		for (Card c:hand){
			counter+=c.getValue();
			if (c.getValue()==1)
				acePresent=true;
			
		}
		if (counter<12 && acePresent)
			counter+=10;
		
		return counter;
	}
	

	/**
	 * Reset the hand of the player for a new round.
	 */
	public void resetHand(){
		this.hand=new ArrayList<Card>();
	}
	
	/**
	 * returns a String representing the hand of the player
	 * @return a String representing the player hand
	 */
	public String showCards(){
		String cards="";
		for (int i =0;i<this.hand.size();i++){
			cards+=hand.get(i).toString()+" ";
		}
		return cards;
	}

}
