package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import player.*;
import cards.*;

/**
 * Implements the function of a single game flow between a dealer and a game
 * First, the init() method is called. It takes care of initiating network objects , then the run() method follows the communication protocol.
 * 
 * 
 * @author Daniele Mucci
 *
 */
public class GameThread extends Thread {
	private final int NUMDECKS = 4;
	private Socket ClientSocket = null;
	private Player dealer = new Player("Dealer");
	private Player customer;
	private Deck deck = new Deck(NUMDECKS);
	private boolean is_new_game = true;
	private PrintWriter out = null;
	private BufferedReader in = null;
	private int bet;
	private boolean double_enabled=false;

	/**
	 * Constructor which takes as parameter the socket to the client to
	 * communicate with it
	 * 
	 * @param s
	 */
	public GameThread(Socket s) {
		this.ClientSocket = s;
	}

	/**
	 * Main method which runs the game.
	 */
	@Override
	public void run() {

		init();
		if (is_new_game) {
			sendREQT("NAME");
			String name = readDATA();
			System.out.println("Name: " + name);
			customer = new Player(name);
		}
		sendREQT("BET");
		bet = Integer.parseInt(readDATA());
		customer.addCard(deck.dealCard()); // Deal a card to the player
		dealer.addCard(deck.dealCard()); // Deal a card to dealer
		customer.addCard(deck.dealCard()); // Deal second card to player
		if (customer.getHandValue()>=9 && customer.getHandValue()<=15)
			double_enabled=true;
		else
			double_enabled=false;
		String cardsToSend = "Your cards are: " + customer.showCards()
				+ " Dealer's hand is: " + dealer.showCards();
		sendDATA(cardsToSend);
		if (customer.getHandValue() == 21)
			blackJack();
		if (double_enabled)
			sendREQT("COMMAND");
		else
			sendREQT("COMMANDNODOUBLE");
		String cmd = readDATA();
		parseCMD(cmd);
		int playerScore = customer.getHandValue();
		int dealerScore = dealer.getHandValue();
		while (dealerScore < 17) {
			Card c = deck.dealCard();
			dealer.addCard(c);
			dealerScore = dealer.getHandValue();
		}
		cardsToSend = "Your cards are: " + customer.showCards()
				+ " with a score of " + playerScore + " Dealer's hand is: "
				+ dealer.showCards() + " with a score of " + dealerScore;
		sendDATA(cardsToSend);
		if (dealerScore>21 ||playerScore > dealerScore)
			win();
		else
			lose();

	}

	/**
	 * Parses and manages the behaviour of the game depending on user's command
	 * . H , S or D
	 * 
	 * @param cmd
	 *            is the command of the user. S for STAND , H for HIT or D for
	 *            DOUBLE
	 */
	private void parseCMD(String cmd) {
		if (cmd.equals("S")) {
			return;
		} else if (cmd.equals("D")) {
			this.bet = 2 * bet;
			Card newCard = deck.dealCard();
			customer.addCard(newCard);
			String cardsToSend = "Your cards are: " + customer.showCards()
					+ " Dealer's card is: " + dealer.showCards();
			sendDATA(cardsToSend);
			if (customer.getHandValue() > 21) {
				lose();
			} else if (customer.getHandValue() == 21) {
				blackJack();
			} else
				return;
		}

		else { // command==HIT

			Card newCard = deck.dealCard();
			customer.addCard(newCard);
			String cardsToSend = "Your cards are: " + customer.showCards()
					+ " Dealer's card is: " + dealer.showCards();
			sendDATA(cardsToSend);
			if (customer.getHandValue() > 21) {
				lose();
				return;
			} else if (customer.getHandValue() == 21) {
				blackJack();
				return;
			}
			sendREQT("COMMANDNODOUBLE");
			String command = readDATA();
			parseCMD(command);
			return;

		}

	}

	/**
	 * Manages the win of the player
	 * 
	 */
	private void win() {

		sendREST("WIN");
		sendREQT("NEW_GAME");
		String decision = readDATA();
		if (decision.equals("YES"))
			resetGame();
		else
			closeGame();
	}
	/**
	 * Manages the win of the player with a blackjack
	 */
	private void blackJack(){
		sendREST("BJ");
		sendREQT("NEW_GAME");
		String decision = readDATA();
		if (decision.equals("YES"))
			resetGame();
		else
			closeGame();
	}

	/**
	 * Manages the loss of the player
	 * 
	 */
	public void lose() {
		sendREST("LOSE");
		sendREQT("NEW_GAME");
		String decision = readDATA();
		if (decision.equals("YES"))
			resetGame();
		else
			closeGame();

	}

	/**
	 * Init method. Initiate the input/output streams
	 */
	private void init() {
		try {
			out = new PrintWriter(this.ClientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					this.ClientSocket.getInputStream()));
		} catch (Exception e) {
			closeGame();
			e.printStackTrace();
		}
	}

	/**
	 * Closing method . close sockets and streams
	 */
	private void closeGame() {
		try {
			this.ClientSocket.close();
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//stop() is generally UNSAFE. In this case tho there are not shared objects
		//With other threads, so its use should be safe
		stop(); 
		return;

	}

	/**
	 * Sends a REQT message. Basically adds REQT header to the payload
	 * 
	 * @param payload
	 *            is the payload of the request. Namely NAME,BET, COMMAND or
	 *            NEW_GAME
	 */

	private void sendREQT(String payload) {
		out.println("REQT" + payload);
	}

	/**
	 * Sends a DATA message. Basically adds DATA header to the payload
	 * 
	 * @param payload
	 *            is the payload of the data. Namely the cards string.
	 */
	private void sendDATA(String payload) {
		out.println("DATA" + payload);
	}

	/**
	 * Sends a REST message. Basically adds REST header to the payload
	 * 
	 * @param payload
	 *            is the payload of the result. Namely WIN or LOSE
	 */
	private void sendREST(String payload) {
		out.println("REST" + payload);
	}

	/**
	 * Reads one DATA message from the socket and returns its payload
	 * @return is the payload of the DATA message.
	 */
	private String readDATA() {
		String dataString = "";
		try {
			dataString = in.readLine();
		} catch (IOException e) {
			closeGame();
			e.printStackTrace();
		}
		String result = dataString.substring(4);
		return result;
	}

	/**
	 * Reset and run the game with the same player.
	 */
	private void resetGame() {
		dealer.resetHand();
		customer.resetHand();
		is_new_game = false;
		this.run();

	}
}
