package server;

import java.io.File;
import java.io.IOException;
import java.security.PrivilegedActionException;
import java.security.Security;

import javax.net.ssl.*;
import javax.swing.JOptionPane;

import com.sun.net.ssl.internal.ssl.Provider;
/**
 * Class which implements main server. This server listens for connection on port 4443. For each client connected a new thread is spawned and a new game is prepared.
 * Therefore games are just 1v1 between dealer and user.
 * Server uses SSL connections, so it needs a certificate . This certificate has been generated for this application with keytool and stored in keystore.ks, with password 'blackjack'.
 * 
 * @author Daniele Mucci
 *
 */
public class Server {

	/**
	 * Main functions. Server listens on port 4443, accepts connections and for each connection create and start a new thread which will manage that single game.
	 * @param args
	 */
	public static void main(String[] args) throws IOException{
		int port=0;
		try {
			port=Integer.parseInt(JOptionPane.showInputDialog("Insert the port to listen (4443)"));
		}
		catch (Exception e) {
			System.out.println("The port was not valid. Shutting down");
			System.exit(1);
		}
		
		Security.addProvider(new Provider());
		System.setProperty("javax.net.ssl.keyStorePassword","blackjack");

		try {
			SSLServerSocketFactory sslServerSocketfactory = (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();
			SSLServerSocket sslServerSocket = (SSLServerSocket)sslServerSocketfactory.createServerSocket(port);
			SSLSocket client=null;
			while (true){
				client = (SSLSocket)sslServerSocket.accept();
				GameThread g=new GameThread(client);
				g.start();
			}
			
		}
		catch(Exception exp)
		{
			PrivilegedActionException priexp = new PrivilegedActionException(exp);
			System.out.println(" Priv exp --- " + priexp.getMessage());

			System.out.println(" Exception occurred .... " +exp);
			exp.printStackTrace();
			System.exit(1);
		}
		
		
		
		
		
	}
}
